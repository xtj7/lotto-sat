import { Component } from '@angular/core';

@Component({
    selector: 'box-layout',
    templateUrl: './box-layout.component.html',
    styleUrls: ['./box-layout.component.scss']
})
export class BoxLayoutComponent {}
