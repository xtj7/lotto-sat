import {Component, ElementRef, Input, OnInit, Renderer2} from '@angular/core';

@Component({
    selector: 'box',
    templateUrl: './box.component.html',
    styleUrls: ['./box.component.scss']
})
export class BoxComponent implements OnInit {
    @Input() span = 1;
    @Input() backgroundImage = '';
    @Input() backgroundColor = '';
    @Input() class = '';
    @Input() title: string;

    constructor(private el: ElementRef, private renderer: Renderer2) {
    }

    ngOnInit(): void {
        if (this.span > 1) {
            this.renderer.setStyle(this.el.nativeElement, 'grid-column-end', 'span ' + this.span);
        }
        if (this.backgroundImage !== '') {
            this.renderer.setStyle(this.el.nativeElement, 'background-image', 'url(' + this.backgroundImage + ')');
        }
        if (this.backgroundColor !== '') {
            this.renderer.setStyle(this.el.nativeElement, 'background-color', this.backgroundColor);
        }
        if (this.class !== '') {
            this.renderer.addClass(this.el.nativeElement, this.class);
        }
    }
}
