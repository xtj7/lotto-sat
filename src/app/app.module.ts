import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {BasketComponent} from './basket/basket.component';
import {LoginComponent} from './login/login.component';
import {BoxComponent} from './shared/box/box.component';
import {BoxLayoutComponent} from './shared/box-layout/box-layout.component';
import {TicketComponent} from './ticket/ticket.component';
import {BasketItemComponent} from './basket/basket-item/basket-item.component';
import {PurchaseCompleteComponent} from './purchase-complete/purchase-complete.component';

@NgModule({
    declarations: [
        AppComponent,
        BoxComponent,
        BoxLayoutComponent,
        HomeComponent,
        BasketComponent,
        BasketItemComponent,
        LoginComponent,
        TicketComponent,
        PurchaseCompleteComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
