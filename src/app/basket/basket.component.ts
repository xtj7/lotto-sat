import {Component} from '@angular/core';
import {IBasketItem} from './basket-item.interface';
import {Router} from '@angular/router';

@Component({
    selector: 'app-basket',
    templateUrl: './basket.component.html',
    styleUrls: ['./basket.component.scss']
})
export class BasketComponent {
    items: IBasketItem[] = [{
        name: 'EuroMillions',
        type: 'euromillones',
        date: 'Fr, 15/02/2019',
        lines: 1,
        ticketNumber: 1234567,
        price: 3.50
    },{
        name: 'Lotto 6 aus 49',
        type: 'lottonormal',
        date: 'Fr, 15/02/2019',
        lines: 1,
        ticketNumber: 1234567,
        price: 3.50
    }];

    constructor(private router: Router) {}

    submitBasket(): void {
        // submit basket here
        this.router.navigateByUrl('/purchase-complete');
    }

    get totalPrice(): number {
        return this.items.reduce((acc, curr) => {
            return acc + curr.price;
        }, 0);
    }
}