export interface IBasketItem {
    name: string;
    type: 'euromillones' | 'lottonormal';
    date: string;
    lines: number;
    ticketNumber: number;
    price: number;
}
