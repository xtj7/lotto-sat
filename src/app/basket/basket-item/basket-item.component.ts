import {Component, Input} from '@angular/core';
import {IBasketItem} from '../basket-item.interface';
import {Router} from '@angular/router';

@Component({
    selector: 'basket-item',
    templateUrl: './basket-item.component.html',
    styleUrls: ['./basket-item.component.scss']
})
export class BasketItemComponent {
    @Input() item: IBasketItem;

    constructor(private router: Router) {}

    deleteItem(ticketNumber: string): void {
        // logic to basket service here
    }

    editItem(ticketNumber: string): void {
        // logic for editing here - maybe not yet? :P
        this.router.navigateByUrl('/euromillions');
    }
}
