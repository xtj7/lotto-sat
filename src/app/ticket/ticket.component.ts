import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-ticket',
    templateUrl: './ticket.component.html',
    styleUrls: ['./ticket.component.scss']
})
export class TicketComponent implements OnInit, OnDestroy {
    private routerDataSubscription: Subscription;
    ticketType: string;

    constructor(private route: ActivatedRoute) {}

    ngOnInit() {
        this.routerDataSubscription = this.route
            .data
            .subscribe(routeData => this.ticketType = routeData.ticket);
    }

    ngOnDestroy() {
        this.routerDataSubscription.unsubscribe();
    }
}
