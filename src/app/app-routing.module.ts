import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {BasketComponent} from './basket/basket.component';
import {HomeComponent} from './home/home.component';
import {TicketComponent} from './ticket/ticket.component';
import {PurchaseCompleteComponent} from './purchase-complete/purchase-complete.component';

const routes: Routes = [
    {path: 'login', component: LoginComponent},
    {path: 'basket', component: BasketComponent},
    {path: 'germanlotto', component: TicketComponent, data: {ticket: 'lottonormal'}},
    {path: 'euromillions', component: TicketComponent, data: {ticket: 'euromillones'}},
    {path: 'purchase-complete', component: PurchaseCompleteComponent},
    {path: '', component: HomeComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
